#include <stdint.h>
#include <string.h>

#include <stdlib.h>
#include "str_utils.h"
#include "testing.h"
#include "htable.h"
#include "vendor/utstring.h"

static void test_simple() {
    TEST(2);

    test("Simple test", 1, "This is false!");
    char res[10]; // 10 = 8 chars + 1 \n + 1 \0 (otherwise it overwrites test_count)
    hash_as_hex(hash_func("test", 4), res);

    STR_START(err)
        test("Compare hash result", strcmp(res, "afd071e5") == 0,
             str_format(err, "Wrong hash value: %s != %s", "afd071e5", res));
    STR_END(err);

    TEST_END
}

static void test_hash_item_create() {
    TEST(1);

    hash_item* item = create_hash_item("test");
    test("Hashed value", item->key == hash_func("test", 4), "Wrong hash value");
    hash_item_free(item);

    TEST_END
}

static void test_hash_table_create() {
    TEST(1);

    hash_table* table = create_hash_table(100);

    STR_START(err)
        test("Initial capacity", table->capacity == 100, str_format(err, "Wrong capacity: %d != 100", table->capacity));
    STR_END(err);

    hash_table_free(table);
    //char *strs;
    //for (int i = 0; i < 8; i++) {
    //strs = (char *)realloc(NULL, 10 * sizeof(char));
      // hash_table_insert(table, strs);
    //}

    TEST_END
}

// TODO: Test that memory is clean after alloc and then extra allocs

static void test_hash_table_insert_and_find() {
    TEST(4);

    hash_table *table = create_hash_table(8);
    STR_START(tr);

    char *strs;
    for (int i = 0; i < 3; i++) {
      strs = (char *)malloc(
          100 *
          sizeof(char)); // set it anything under HTABLE_ITEM_VAL_MAX and it
                         // will throw a malloc error (because we are trying to
                         // alloc over existing alloc'd memory)
      sprintf(strs, "test%d", i);
      hash_table_insert(table, strs);

      test(str_format(tr, "Size of hash table must be %d", i + 1),
           table->size == i + 1, "Not correct size after insertion");

      hash_item* item = hash_table_find(table, strs);
      test("Value matches when found", strcmp(item->value, strs) == 0,
           "Values don't match");
    }

    STR_END(tr);

    hash_table_free(table);

  TEST_END
}

// NOTE: If load factor changes, this test needs to change.
static void test_hash_table_realloc() {
    TEST(4);

    hash_table *table = create_hash_table(8);

    char *strs;
    for (int i = 0; i < 8; i++) {
      strs = (char *)malloc(
          HTABLE_ITEM_VAL_MAX *
          sizeof(char)); //set it anything under HTABLE_ITEM_VAL_MAX and it
                         //will throw a malloc error (because we are trying to
                         //alloc over existing alloc'd memory)
      sprintf(strs, "test%d", i);
      hash_table_insert(table, strs);
    }

    test("Is new capacity", table->capacity == 16, "Wrong capacity");

    hash_table_free(table);

    TEST_END
}

// Tests that every entry has a unique value. It was overwriting in earlier
// tests, because the pointer was the same in each loop iteration (compile optimization?)
void test_hash_table_no_mem_overwrites() {

    TEST(24);

    hash_table *table = create_hash_table(8);

    char *strs;
    for (int i = 0; i < 8; i++) {
      strs = (char *)malloc(
          HTABLE_ITEM_VAL_MAX *
          sizeof(char)); //set it anything under HTABLE_ITEM_VAL_MAX and it
                         //will throw a malloc error (because we are trying to
                         //alloc over existing alloc'd memory)
      sprintf(strs, "test%d", i);
      hash_table_insert(table, strs);
    }

    for (int i = 0; i < 8; i++) {
      strs = (char *)malloc(
          HTABLE_ITEM_VAL_MAX * sizeof(char));
      sprintf(strs, "test%d", i);
      test("Is the value matching?", hash_table_find(table, strs),
           "Strings don't match");
    }

    for (int i = 8; i < 24; i++) {
      strs = (char *)malloc(
          HTABLE_ITEM_VAL_MAX * sizeof(char));
      sprintf(strs, "test%d", i);
      hash_table_insert(table, strs);
    }

    for (int i = 8; i < 24; i++) {
      strs = (char *)malloc(
          HTABLE_ITEM_VAL_MAX * sizeof(char));
      sprintf(strs, "test%d", i);
      test("Is the value matching?", hash_table_find(table, strs),
           "Strings don't match");
    }

    TEST_END
}

// TODO
void test_hash_table_reindexes_on_realloc() {

    TEST(8);

    int pos[8][2];
    char *strs;
    hash_table *table = create_hash_table(8);

    for (int i = 0; i < 8; i++) {
        strs = (char *)malloc(
            HTABLE_ITEM_VAL_MAX * sizeof(char));
        sprintf(strs, "test%d", i);
        hash_table_insert(table, strs);
        pos[i][0] = i;
        pos[i][1] = hash_func(strs, strlen(strs));
    }

    for (int i = 8; i < 16; i++) {
        strs = (char *)malloc(
            HTABLE_ITEM_VAL_MAX * sizeof(char));
        sprintf(strs, "test%d", i);
        hash_table_insert(table, strs);
    }

    for (int i = 0; i < 8; i++) {
        strs = (char *)malloc(HTABLE_ITEM_VAL_MAX * sizeof(char));
        sprintf(strs, "test%d", i);
        hash_item *item = hash_table_find(table, strs);
        test("if string is different", hash_func(strs, 5), "string is not different");
    }

    TEST_END;
}

// Function to generate a string that would enter in the same index as the string provided.
char *find_a_char_to_collide_with(char *first, int length, int capacity) {
    uint32_t first_hash = hash_func(first, length) % capacity;
    char *second = (char *)realloc(NULL, length);
    do {
        char *second_ptr = second;
        for (int i = 0; i < length-1; i++) {
            *second_ptr = (rand() % 26) + '0';
            second_ptr++;
        }
        *second_ptr = '\0';

        uint32_t second_hash = hash_func(second, length) % capacity;
        if (first_hash == second_hash) {
            return second;
        }
    } while (1);

    return NULL;
}

void test_collision_finder() {
    TEST(3);
    test("Found a colliding string?",
         find_a_char_to_collide_with("test", 5, 2) != NULL,
         "Not found 1. collision");
    test("Found a colliding string?",
         find_a_char_to_collide_with("foobar", 7, 4) != NULL,
         "Not found 2. collision");
    test("Found a colliding string?",
         find_a_char_to_collide_with("foobarbaz", 10, 8) != NULL,
         "Not found 3. collision");
    TEST_END;
}

void test_hash_table_handle_collisions() {
    TEST(2 * 16);

    int capacity = 8;
    int test_size = 7;
    hash_table *table = create_hash_table(capacity);
    printf("%p\n", (void *)table);
    char *first = (char *)malloc(sizeof(char) * test_size);

#ifdef DEBUG
    printf("%p\n", (void *)first);
#endif

    strcpy(first, "foobar");

#ifdef DEBUG
    printf("%p\n", (void *)first);
#endif


    int i = 2;
    int step = 0;
    int wait_to_step = 0;
    char *collider;
    do {
        if ((float)(i) / table->capacity == HTABLE_LOAD_FACTOR) wait_to_step = 1;

        collider = find_a_char_to_collide_with(first, test_size, table->capacity);

#ifdef DEBUG
        printf("%p\n", (void *)collider);
#endif


        hash_table_insert(table, first);
        hash_table_insert(table, collider);

        printf("step: %d, \% : %f, iter: %d, cap: %d, %d == %d\n", step, (float)(i) / table->capacity, i, table->capacity, table->size, i + step);
        test("Did the size stay the same?", table->size == i + step, "The size is not the same");
        hash_item check = table->items[hash_func("foobar", test_size) % table->capacity];
        test("Did it not collide?", strcmp(collider, check.value) != 0, "They collided!");
        i++;
        // step = table->capacity / capacity == 0 ? step + 1 : step;
        first = collider;
        collider = find_a_char_to_collide_with(first, test_size, table->capacity);

#ifdef DEBUG
        VIEW_TABLE(table);
#endif

        if (wait_to_step == 1) {
            step++;
            wait_to_step = 0;
        }

    } while(i < 16);

    free(first);
    free(collider);
    hash_table_free(table);

    TEST_END;
}

int main() {
    test_simple();
    test_hash_item_create();
    test_hash_table_create();

    test_hash_table_insert_and_find();
    test_hash_table_realloc();
    test_hash_table_no_mem_overwrites();
    test_hash_table_reindexes_on_realloc();
    test_collision_finder();
    test_hash_table_handle_collisions();

    show_total_success();
    show_total_failure();
    finish_tests();
}
