#!/usr/bin/env bash

log_from=$1
log_to=$2
path=$3
bucket_start=$4
bucket_increment=$5
curr_bucket=$bucket_start
count=0
logged=0

# get all the times for a specific path in the original log
times=(); tcount=0
for time in $(grep $path $log_from | cut -d'|' -f2 | sort); do
    times+=("$time")
    tcount=$(($tcount+1))
done

last_time=${times[$(($tcount-1))]}

# create a list of buckets from the start range to the end of the original log
buckets=($bucket_start)
bcount=1
while true; do
    incremented=$((${buckets[$bcount-1]}+$bucket_increment))
    if [[ "$incremented" -gt "$last_time" ]]; then
        break;
    fi

    buckets[$bcount]="$incremented"
    bcount=$(($bcount+1))
done


# each entry aligns with each bucket like:
# 1000|10 (time|count)
# 1600|5
results=()
timepos=0
resultpos=0
for bucket in ${buckets[@]}; do
    resultcount=0
    for time in ${times[@]:$timepos}; do
        from=$bucket
        to=$(($bucket+$bucket_increment))
        if [[ "$time" -ge "$from" && "$time" -lt "$to"  ]]; then
            resultcount=$(($resultcount+1))
            timepos=$(($timepost+1))
            results[$resultpos]=$resultcount
        fi
    done

    results[$resultpos]=$resultcount
    resultpos=$(($resultpos+1))
    timepos=$(($timepost+1))
    resultcount=0
done

# print to the bucket log
for line in $(seq 0 $((${#buckets[*]}-1))); do
    if [[ "${results[$line]}" == "0" ]]; then
        continue
    fi
    echo "${buckets[$line]}|${results[$line]}" >> $log_to
done

if [[ $DEBUG == 1 ]]; then
    echo times
    printf '%s-' "${times[@]}"
    echo
    echo tcount
    echo $tcount
    echo
    echo last_time
    echo $last_time
    echo
    echo buckets
    printf '%s-' "${buckets[@]}"
    echo
    echo "BUCKET SIZE - ${#buckets[*]}"
    echo
    echo results
    printf '%s-' "${results[@]}"
    echo
    echo "RESULT SIZE - ${#buckets[*]}"
fi
