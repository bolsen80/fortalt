#!/usr/bin/env bash

log=$1
path=$2
from=$3
to=$4
count=0
yes=0

for time in $(grep $path $log | cut -d'|' -f2 | sort); do
    if [[ "$time" -ge "$from" && "$time" -le "$to"  ]]; then
        count=$((count+1))
        yes=1
    else if [[ "$yes" == "1" ]]; then
            # if yes=1 and the earlier condition didn't hold, we can stop looking
            # since the file was pre-sorted before the loop started
            break
        fi
    fi
done

echo $count
