#ifndef _STR_UTILS_H
#define _STR_UTILS_H

#include "vendor/utstring.h"
#include <string.h>

static inline char *str_format(UT_string *str, const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  utstring_new(str);
  utstring_printf_va(str, fmt, ap);
  va_end(ap);

  char *res = utstring_body(str);
  char *up;

  return res;
}

#define STR_START(var_name) UT_string *var_name; utstring_new(var_name);

#define STR_END(VAR) utstring_free(VAR);

#endif
