#!/bin/bash

FG_GREEN="$(tput setaf 2)";
FG_RED="$(tput setaf 1)";
FG_BOLD="$(tput bold)"
OFF="$(tput sgr0)"

IFS=$'\n'
res=`$1`
code=$?

echo "$res" | sed "s/^\(test_.*\)/${FG_BOLD}\1${OFF}/g;;s/^ok/${FG_GREEN}${FG_BOLD}ok${OFF}/g;;s/not ok/${FG_RED}${FG_BOLD}not ok${OFF}/g"

if [[ $code == "139" ]]; then
    echo "${FG_BOLD}Exited with error code 139: Got a segfault!!${OFF}"
fi


exit $code
