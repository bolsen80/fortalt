#ifndef _TESTING_H
#define _TESTING_H

#include <assert.h>
#include <stdlib.h>
#include "str_utils.h"

int total_success = 0;
int total_failure = 0;
#define TEST(harness_count)                                             \
    ({                                                                  \
    setvbuf(stdout,(char *)_IONBF,0,0);                                 \
    int test_count = 0;                                                 \
    ((void)printf("%s\n", __ASSERT_FUNCTION), (void)printf("1..%d\n", harness_count));

#define TEST_END                                                        \
    test_count = 0;                                                    \
    (void)printf("\n");                                                \
});

#define test(test_name, e, msg)                                         \
  ((void)((e) ? __ok(test_name, ++test_count)                                  \
              : __notok(test_name, #e, __FILE__, __LINE__, msg,                \
                        ++test_count)))

#define __ok(test_name, count)                                                 \
    (++total_success, (void)printf("ok %d - %s\n", count, test_name))
#define __notok(test_name, e, file, line, msg, count)                          \
    (++total_failure, (void)printf("not ok - %d : %s - %s:%u: failed assertion `%s: %s'\n", \
                count, test_name, file, line, e, msg),                         \
   0)

#define show_total_success()                                                   \
  ((void)printf("Total successes: %d tests succeeded\n", total_success))
#define show_total_failure()                                                   \
  ((void)printf("Total failures: %d tests failed\n", total_failure))
#define finish_tests()                          \
    (exit(total_failure == 0 ? 0 : 1))

#endif
