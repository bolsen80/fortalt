# Fortalt

"fortalt" is the present perfect form of "told" in Norwegian. "Jeg har fortalt det som teller."

Fortalt is a dumb little analytics database. It's supposed to take events of things and organize and count them quick enough. It's dumb because I am mostly implementing it mostly naively (lazy first with things, then maybe use some better algorithms for large data).

It's also a toy - there are better columnar OLAP database systems out there. I am doing this because I had exposure to ideas in OLAP database systems at a high-level and I wondered a lot how they worked.

I think processing large files in various formats and doing large enough data analysis outside an RDBMS is a life-skill for software developers who have to munge large amounts of data (for example, the archetypal "backend" developer, where the target product is a web page), something I want to improve on.

The project is a mix of bash and C. Bash is being used for quick prototyping and then also for gluing small C programs together when necessary.

# What does it do now?

Assume that this is used to do counts of pages on a web site. For each web page, we can give an input like:

```
fortalt put /path/to/file 1697708181
```

If we have a "grid" of times aligned to X minute intervals (X can default to 5 minutes), we can bucket the "hit" to `/path/to/file` in a bucket between 1135 to 1140 (which is when I took the example time with `date +'%s'`.

There could be some structure then, where we key it on something, like the path:

``` yaml
/path/to/file:
  1697708000: 10
  1697708600: 10
/my-article.html:
  1697708000: 5
  1697708600: 20
  1697708700: 10
```

(It's not stored in YAML, just here for demonstration.)

Another tool can then give me an aggregate over a longer time period:

```
fortalt aggr "/my-article.html" 1697708000 1697708700
```

This will produce a single value of 35, using the example above.

I can bucket times:

```
fortalt bucket log testbucket "/test" 1697736400 600
```

This is basically bucketing all the events under "/test" in 5-minute intervals starting at some time.

# License

Copyright 2023 Brian Olsen. Licensed under AGPL.
