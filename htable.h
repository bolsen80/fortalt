#ifndef _HTABLE_H
#define _HTABLE_H

#include <stdint.h>
#include <stdio.h>

// resize if HTABLE_LOAD_FACTOR * size >= capacity
#define HTABLE_LOAD_FACTOR 0.75

// limit the size to strings to 512 bytes
#define HTABLE_ITEM_VAL_MAX 512

#define GROW_CAPACITY(capacity)                 \
    ((capacity) < 8 ? 8 : (capacity) * 2);

#define ALLOCATE(type, count)                           \
    (type *)reallocate(NULL, 0, sizeof(type) * (count))

#define FREE_ARRAY(type, pointer, oldCount)             \
    reallocate(pointer, sizeof(type) * (oldCount), 0)

#define VIEW_TABLE(table)                                               \
    do {                                                                \
        printf("--------------------------\n");                         \
        printf("Size: %d, Capacity: %d\n", table->size, table->capacity); \
        printf("--------------------------\n");                         \
        for (int i = 0; i < (table)->capacity; i++) {                   \
            printf("%d KEY == %d | VALUE == %s (%p)\n", i, (table)->items[i].key, \
                   (int)(table)->items[i].key == -1 ? "<empty>"         \
                   : (table)->items[i].value, (void *)(&table->items[i])); \
        }                                                               \
    } while(0);

typedef struct {
    uint32_t key;
    char* value;
} hash_item;

typedef struct {
    int size;
    int capacity;
    hash_item* items;
} hash_table;

uint32_t hash_func(const char *key, int length);
void hash_as_hex(uint32_t hash, char *res);
hash_item* create_hash_item(char* value);
void hash_item_free(hash_item* item);
hash_table* create_hash_table(int initial_capacity);
hash_item* hash_table_find(hash_table *table, char* value);
void hash_table_insert(hash_table *table, char* value);
void hash_table_delete(hash_table * table, hash_item * hi);
void hash_table_persist(
    hash_table *table,
    FILE * file); // will "serialize" the data to a file.
// Naively!-serialization will end up being
// architecture-specific and probably compiler-specific.
void hash_table_free(hash_table *table);
void *reallocate(void *pointer, size_t oldSize, size_t newSize);

#endif
