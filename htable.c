#include "htable.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * This is a really simple hash table where it maps the hash as key to the
 * actual string.
 *
 * For example: if I had "test", and the hash function returned 5 for it, the
 * hash item would be 5 -> test.
 *
 * This is for "string interning" primarily. Every string would be passed
 * through here either to check or to intern.
 *
 * I am influenced by https://craftinginterpreters.com/hash-tables.html, but I
 * simplified it to focus just on string interning.
 */

// This is FNV-1a
uint32_t hash_func(const char *key, int length) {
  uint32_t hash = 2166136261u;
  for (int i = 0; i < length; i++) {
    hash ^= (uint8_t)key[i];
    hash *= 16777619;
  }
  return hash;
}

// TODO: We should clean up instead of aborting
#define FAIL(msg)                                                       \
    do {                                                                \
        fprintf(stderr, msg);                                           \
        abort();                                                        \
    } while(0);                                                         \


inline void hash_as_hex(uint32_t hash, char *res) {
  sprintf(res, "%lx", (long unsigned int)hash);
}

static void hash_item_init(hash_item *item, char *value) {
  size_t value_size = strlen(value);
  if (value_size > HTABLE_ITEM_VAL_MAX) {
    value_size = HTABLE_ITEM_VAL_MAX;
  }
  memcpy(item->value, value, value_size);
  item->key = hash_func(value, strlen(value));
}

hash_item *create_hash_item(char *value) {
  hash_item *item = (hash_item *)reallocate(NULL, 0, sizeof(hash_item));
  if (!item) FAIL("Could not create hash item");
  item->value = (char *)malloc(sizeof(char));
  hash_item_init(item, value);
  return item;
}

void hash_item_free(hash_item *item) {
  FREE_ARRAY(hash_item, item->value, 0);
  reallocate(item, 1, 0);
}

static void adjust_capacity(hash_table *table, int capacity);

static void hash_table_init(int capacity, int size, hash_table *table) {
  table->capacity = capacity;
  table->size = 0;
  adjust_capacity(table, capacity);

  // checking in this in: this allocated it wrong and succeeding mallocs wrote over memory
  //hash_item *tmp = (hash_item *)reallocate(NULL, capacity, sizeof(hash_item));
  //if (!tmp) FAIL("Could not allocate new hash item in new hash table");
  //table->items = tmp;
  //for (int i = 0; i < capacity; i++) {
//    table->items[i].key = -1;
    //table->items[i].value = NULL;
  //  }
}

hash_table *create_hash_table(int initial_capacity) {
  hash_table *table = (hash_table *)reallocate(NULL, 0, sizeof(hash_table) * initial_capacity);
  if (!table) FAIL("Could not allocate hash table");
  hash_table_init(initial_capacity, 0, table);
  return table;
}

void hash_table_free(hash_table *table) {
  FREE_ARRAY(hash_item, table->items, table->capacity);
  free(table);
}

static void adjust_capacity(hash_table *table, int capacity) {
  hash_item *items = ALLOCATE(hash_item, capacity);
  for (int i = 0; i < capacity; i++) {
    items[i].key = -1;
    items[i].value = "";
  }

  table->items = items;
  table->capacity = capacity;
}

hash_item *hash_table_find(hash_table *table, char *value) {
  uint32_t key = hash_func(value, strlen(value) + 1);
  uint32_t index = hash_func(value, strlen(value) + 1) % table->capacity;
  for (;;) {
    hash_item *item = &table->items[index];
    if (item->key == key || (int)item->key == -1) {
      return item;
    }
    index = (index + 1) % table->capacity; // it will try to loop around, but without table resizes this loop would never end
  }
}

void hash_table_insert(hash_table *table, char *value) {
  if (table->size + 1 > table->capacity * HTABLE_LOAD_FACTOR) {
    int capacity = GROW_CAPACITY(table->capacity);
    adjust_capacity(table, capacity);
  }

  hash_item *item = hash_table_find(table, value);
  bool is_new = (int)item->key == -1;
  if (is_new) {
    table->size++;
  }
  item->key = hash_func(value, strlen(value) + 1);
  item->value = value;
}

void *reallocate(void *pointer, size_t oldSize, size_t newSize) {
  void *result = realloc(pointer, newSize);

  if (newSize == 0) {
    return NULL;
  }

  //> out-of-memory
  if (result == NULL) FAIL("realloc() returned NULL");
  //< out-of-memory
  return result;
}
